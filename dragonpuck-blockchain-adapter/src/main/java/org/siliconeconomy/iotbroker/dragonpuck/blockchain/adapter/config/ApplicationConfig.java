/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.AdapterIdentifierProvider;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration of various application beans.
 *
 * @author M. Grzenia
 */
@Configuration
@EnableConfigurationProperties(DeviceIntegrationProperties.class)
@Import({
    AdapterIdentifierProvider.class,
    DeviceTypeProvider.class
})
public class ApplicationConfig {

  /**
   * Configures the {@link ObjectMapper} bean to use.
   * <p>
   * This instance is primarily used for deserializing/serializing {@link SensingPuckMessageDtoBase}
   * instances (received via AMQP/when publishing to the Blockchain Connector).
   *
   * @return An {@link ObjectMapper}.
   */
  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new JavaTimeModule());
  }
}
