/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * The application's starting point.
 *
 * @author M. Grzenia
 */
@SpringBootApplication
@ConfigurationPropertiesScan
public class DragonPuckBlockchainAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(DragonPuckBlockchainAdapterApplication.class);
    }
}
