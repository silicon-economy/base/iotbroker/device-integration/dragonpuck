/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.DeviceMessageListener;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.sensingpuck.adapter.DeviceTypeProvider;
import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.siliconeconomy.iotbroker.utils.amqp.DeviceMessageExchangeConfiguration;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of various AMQP related beans.
 *
 * @author M. Grzenia
 */
@Configuration
public class AmqpConfig {

  private final DeviceTypeProvider deviceTypeProvider;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceTypeProvider          Provides information about the adapter's {@link DeviceType}.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public AmqpConfig(DeviceTypeProvider deviceTypeProvider,
                    DeviceIntegrationProperties deviceIntegrationProperties) {
    this.deviceTypeProvider = requireNonNull(deviceTypeProvider, "deviceTypeProvider");
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
  }

  /**
   * Configures (and creates) the AMQP exchange where {@link SensingPuckMessageDtoBase} messages are
   * published to.
   *
   * @return The configured {@link Exchange}.
   */
  @Bean
  public Exchange deviceMessageExchange() {
    return ExchangeBuilder
        .topicExchange(DeviceMessageExchangeConfiguration.NAME)
        .durable(DeviceMessageExchangeConfiguration.DURABLE)
        .build();
  }

  /**
   * Configures (and creates) the {@link Queue} used by {@link DeviceMessageListener}.
   *
   * @return The configured {@link Queue}.
   */
  @Bean
  public Queue dragonPuckMessageQueue() {
    return QueueBuilder
        // Use a fixed name for the queue to ensure that it is shared between multiple adapter
        // instances. This way, a (single) message is distributed to only one adapter instance.
        // (We want each message to be published to the connector only once.)
        .nonDurable("dragonpuck-blockchain-adapter-v2")
        .autoDelete()
        .build();
  }

  /**
   * Configures the {@link Binding} from the {@link #dragonPuckMessageQueue()} to the
   * {@link #deviceMessageExchange()}.
   *
   * @return The configured {@link Binding}.
   */
  @Bean
  public Binding dragonPuckMessageBinding() {
    // For this binding we are interested in messages form the adapter's device type and the
    // configured device source.
    String routingKey = DeviceMessageExchangeConfiguration.formatDeviceMessageRoutingKey(
        deviceTypeProvider.getAdapterDeviceTypeIdentifier(),
        deviceIntegrationProperties.getDeviceSource(),
        "*"
    );
    return BindingBuilder
        .bind(dragonPuckMessageQueue())
        .to(deviceMessageExchange())
        .with(routingKey)
        .noargs();
  }

  /**
   * Configures the {@link RabbitListenerContainerFactory} to use.
   * <p>
   * Spring's default {@link RabbitListenerContainerFactory} uses the
   * {@link SimpleMessageConverter} which is not capable of deserializing JSON messages received
   * via AMQP. Because of this, a custom and properly configured {@link MessageConverter} is
   * required. We could define the message converter as a bean and Spring would 'automagically' set
   * and use it, but this way it's more explicit. Also, for methods annotated with
   * {@link RabbitListener}, this way we don't have to explicitly specify the message converter to
   * use.
   *
   * @param connectionFactory The {@link ConnectionFactory} instance to use for creating
   *                          connections.
   * @param objectMapper      The {@link ObjectMapper} instance to use for deserializing JSON
   *                          messages.
   * @return The configured {@link RabbitListenerContainerFactory}.
   */
  @Bean
  public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
      ConnectionFactory connectionFactory,
      ObjectMapper objectMapper) {
    var messageConverter = new Jackson2JsonMessageConverter(objectMapper);

    var factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setMessageConverter(messageConverter);

    return factory;
  }
}
