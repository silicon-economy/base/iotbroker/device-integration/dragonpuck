/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.function.client.WebClient;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of the {@link WebClient}-specific beans.
 *
 * @author M. Grzenia
 */
@Configuration
public class WebClientConfig {

  private final BlockchainAdapterProperties blockchainAdapterProperties;

  /**
   * Creates a new instance.
   *
   * @param blockchainAdapterProperties The adapter-relevant (configuration) properties.
   */
  public WebClientConfig(BlockchainAdapterProperties blockchainAdapterProperties) {
    this.blockchainAdapterProperties = requireNonNull(blockchainAdapterProperties,
                                                      "blockchainAdapterProperties");
  }

  /**
   * Configures the {@link WebClient} to use for accessing the web API of the Dragon Puck's
   * Blockchain Connector.
   *
   * @param builder      The {@link WebClient.Builder} to use for creating the {@link WebClient}.
   * @param objectMapper The {@link ObjectMapper} to use for serializing objects to their JSON
   *                     representation when publishing to the Blockchain Connector.
   * @return A {@link WebClient}.
   */
  @Bean
  public WebClient blockchainConnectorClient(WebClient.Builder builder,
                                             ObjectMapper objectMapper) {
    return builder
        .baseUrl(blockchainAdapterProperties.getBlockchainConnectorApiUrl())
        .codecs(clientCodecConfigurer -> clientCodecConfigurer
            .defaultCodecs()
            .jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper))
        )
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .build();
  }
}
