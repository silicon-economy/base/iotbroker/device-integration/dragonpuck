/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter;

import org.siliconeconomy.iotbroker.sensingpuck.common.config.DeviceIntegrationProperties;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static java.util.Objects.requireNonNull;

/**
 * Provides methods for accessing the Dragon Puck's Blockchain Connector through its web API.
 *
 * @author M. Grzenia
 */
@Service
public class BlockchainConnectorService {

  private static final Logger LOG = LoggerFactory.getLogger(BlockchainConnectorService.class);
  private final WebClient webClient;
  private final DeviceIntegrationProperties deviceIntegrationProperties;

  /**
   * Creates a new instance.
   *
   * @param webClient                   The client to use for request to the Blockchain Connector's
   *                                    web API.
   * @param deviceIntegrationProperties The configuration properties related to the devices being
   *                                    integrated.
   */
  public BlockchainConnectorService(@Qualifier("blockchainConnectorClient") WebClient webClient,
                                    DeviceIntegrationProperties deviceIntegrationProperties) {
    this.webClient = requireNonNull(webClient, "webClient");
    this.deviceIntegrationProperties = requireNonNull(deviceIntegrationProperties,
                                                      "deviceIntegrationProperties");
  }

  /**
   * Publishes a message to the Blockchain Connector to persist the device data to the blockchain
   * network.
   *
   * @param message The message to publish.
   */
  public void publishMessage(SensingPuckMessageDtoBase message) {
    LOG.debug("Publishing message: {}", message);
    webClient.post()
        .uri(uriBuilder -> uriBuilder
            .path("devices/{source}/publish")
            .build(deviceIntegrationProperties.getDeviceSource()))
        .bodyValue(message)
        .retrieve()
        .bodyToMono(Void.class)
        .onErrorResume(ex -> {
          LOG.warn("Failed to publish message: {}", message, ex);
          // Don't propagate errors any further.
          return Mono.empty();
        })
        .block();
  }
}
