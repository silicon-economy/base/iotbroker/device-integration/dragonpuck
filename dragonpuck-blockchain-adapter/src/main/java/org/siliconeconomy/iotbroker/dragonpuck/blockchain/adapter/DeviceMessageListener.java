/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter;

import org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config.AmqpConfig;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

/**
 * Configures a {@link RabbitListener} for {@link SensingPuckMessageDtoBase} messages.
 *
 * @author M. Grzenia
 */
@Component
public class DeviceMessageListener {

  private static final Logger LOG = LoggerFactory.getLogger(DeviceMessageListener.class);
  private final BlockchainConnectorService blockchainConnectorService;

  /**
   * Creates a new instance.
   *
   * @param blockchainConnectorService Provides methods for accessing the Dragon Puck's
   *                                   Blockchain Connector through its web API.
   */
  public DeviceMessageListener(BlockchainConnectorService blockchainConnectorService) {
    this.blockchainConnectorService = requireNonNull(blockchainConnectorService,
                                                     "blockchainConnectorService");
  }

  /**
   * The {@link RabbitListener} callback method for {@link SensingPuckMessageDtoBase} messages
   * published via the IoT Broker's AMQP broker.
   *
   * @param message The {@link SensingPuckMessageDtoBase} message received via AMQP.
   * @see AmqpConfig
   */
  @RabbitListener(queues = "#{dragonPuckMessageQueue}")
  public void onDeviceMessage(SensingPuckMessageDtoBase message) {
    LOG.debug("Received device message: {}", message);
    blockchainConnectorService.publishMessage(message);
  }
}
