/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import static java.util.Objects.requireNonNull;

/**
 * Defines adapter-relevant (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "blockchainadapter")
@ConstructorBinding
@Getter
public class BlockchainAdapterProperties {

  private final String blockchainConnectorApiUrl;

  /**
   * Creates a new instance.
   *
   * @param blockchainConnectorApiUrl The URL to the Blockchain Connector's web API.
   */
  public BlockchainAdapterProperties(String blockchainConnectorApiUrl) {
    this.blockchainConnectorApiUrl = requireNonNull(blockchainConnectorApiUrl,
                                                    "blockchainConnectorApiUrl");
  }
}
