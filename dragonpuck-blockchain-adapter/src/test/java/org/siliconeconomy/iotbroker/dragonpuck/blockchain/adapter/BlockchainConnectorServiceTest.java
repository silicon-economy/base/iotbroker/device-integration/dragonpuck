/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config.BlockchainAdapterProperties;
import org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter.config.WebClientConfig;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.CommCause;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.FirmwareDescriptor;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.common.message.MotionState;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v1.message.V1SensingPuckMessageDto;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingData;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.v2.message.V2SensingPuckMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;

/**
 * Test cases for {@link BlockchainConnectorService}.
 *
 * @author M. Grzenia
 */
@SpringBootTest
@TestPropertySource("/application-test-adapter.properties")
class BlockchainConnectorServiceTest {

  /**
   * Class under test.
   */
  @Autowired
  private BlockchainConnectorService service;
  /**
   * Test environment.
   */
  private static MockWebServer mockBackend;

  @BeforeAll
  static void beforeAll()
      throws IOException {
    mockBackend = new MockWebServer();
    mockBackend.start();
  }

  @AfterAll
  static void afterAll()
      throws IOException {
    mockBackend.shutdown();
  }

  @Test
  void publishMessage_whenV1Message_thenPublishesCorrespondingJson()
      throws Exception {
    // Arrange
    mockBackend.enqueue(new MockResponse().setResponseCode(HttpStatus.CREATED.value()));
    V1SensingPuckMessageDto message = new V1SensingPuckMessageDto(
        1,
        42,
        7,
        new FirmwareDescriptor("1.0.0", 0, true, 4711, false),
        Instant.parse("2021-01-01T12:00:00Z"),
        CommCause.DATA,
        List.of(
            new V1SensingData(23.0f,
                              45.0f,
                              Instant.parse("2021-01-01T11:00:00Z"),
                              MotionState.STATIC)
        )
    );

    // Act
    service.publishMessage(message);

    // Assert
    RecordedRequest recordedRequest = mockBackend.takeRequest();
    assertThat(recordedRequest.getMethod()).isEqualTo("POST");
    assertThat(recordedRequest.getPath()).isEqualTo("/devices/dragonpuck/publish");
    assertThat(recordedRequest.getBody().readUtf8())
        .isEqualTo(
            "{\"protocolVersion\":1,\"jobId\":42,\"deviceId\":7," +
                "\"firmwareDescriptor\":{\"versionNumber\":\"1.0.0\",\"commitCounter\":0," +
                "\"releaseFlag\":true,\"commitHash\":4711,\"dirtyFlag\":false}," +
                "\"comTimestamp\":\"2021-01-01T12:00:00Z\",\"lastComCause\":\"Data\"," +
                "\"data\":[{\"temperature\":23.0,\"humidity\":45.0," +
                "\"timestamp\":\"2021-01-01T11:00:00Z\",\"motionState\":\"Static\"}]}"
        );
  }

  @Test
  void publishMessage_whenV2Message_thenPublishesCorrespondingJson()
      throws Exception {
    // Arrange
    mockBackend.enqueue(new MockResponse().setResponseCode(HttpStatus.CREATED.value()));
    V2SensingPuckMessageDto message = new V2SensingPuckMessageDto(
        2,
        7,
        42,
        new FirmwareDescriptor("1.0.0", 0, true, 4711, false),
        Instant.parse("2021-01-01T12:00:00Z"),
        CommCause.DATA,
        List.of(
            new V2SensingData(23.0f,
                              45.0f,
                              71.0f,
                              Instant.parse("2021-01-01T11:00:00Z"),
                              MotionState.STATIC)
        )
    );

    // Act
    service.publishMessage(message);

    // Assert
    RecordedRequest recordedRequest = mockBackend.takeRequest();
    assertThat(recordedRequest.getMethod()).isEqualTo("POST");
    assertThat(recordedRequest.getPath()).isEqualTo("/devices/dragonpuck/publish");
    assertThat(recordedRequest.getBody().readUtf8())
        .isEqualTo(
            "{\"protocolVersion\":2,\"jobId\":7,\"deviceId\":42," +
                "\"firmwareDescriptor\":{\"versionNumber\":\"1.0.0\",\"commitCounter\":0," +
                "\"releaseFlag\":true,\"commitHash\":4711,\"dirtyFlag\":false}," +
                "\"comTimestamp\":\"2021-01-01T12:00:00Z\",\"lastComCause\":\"Data\"," +
                "\"data\":[{\"temperature\":23.0,\"humidity\":45.0,\"voltage\":71.0," +
                "\"timestamp\":\"2021-01-01T11:00:00Z\",\"motionState\":\"Static\"}]}"
        );
  }

  @Test
  void publishMessage_whenPublishFails_thenDoesNotPropagateException()
      throws Exception {
    // Arrange
    mockBackend.enqueue(new MockResponse().setResponseCode(HttpStatus.REQUEST_TIMEOUT.value()));
    V1SensingPuckMessageDto message = new V1SensingPuckMessageDto();
    message.setProtocolVersion(1337);

    // Act & Assert
    assertThatNoException().isThrownBy(() -> service.publishMessage(message));
    // Cleanup - Subsequent tests would otherwise process this method's request.
    mockBackend.takeRequest();
  }

  /**
   * Overrides the default {@link WebClientConfig}.
   */
  @TestConfiguration
  public static class TestWebClientConfig
      extends WebClientConfig {

    public TestWebClientConfig(BlockchainAdapterProperties blockchainAdapterProperties) {
      super(blockchainAdapterProperties);
    }

    @Override
    public WebClient blockchainConnectorClient(WebClient.Builder builder,
                                               ObjectMapper objectMapper) {
      WebClient webClient = super.blockchainConnectorClient(builder, objectMapper);
      // For the test we want to use the mocked backend's URL as the base URL.
      String baseUrl = String.format("http://localhost:%s", mockBackend.getPort());
      return webClient.mutate().baseUrl(baseUrl).build();
    }
  }
}
