/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.dragonpuck.blockchain.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.sensingpuck.common.model.SensingPuckMessageDtoBase;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Test cases for {@link DeviceMessageListener}.
 *
 * @author M. Grzenia
 */
class DeviceMessageListenerTest {

  /**
   * Class under test.
   */
  private DeviceMessageListener deviceMessageListener;
  /**
   * Test dependencies.
   */
  private BlockchainConnectorService blockchainConnectorService;

  @BeforeEach
  void setUp() {
    blockchainConnectorService = mock(BlockchainConnectorService.class);
    deviceMessageListener = new DeviceMessageListener(blockchainConnectorService);
  }

  @Test
  void onDeviceMessage_shouldForwardMessageToBlockchainConnectorService() {
    // Arrange
    SensingPuckMessageDtoBase message = new SensingPuckMessageDtoBase(1);

    // Act
    deviceMessageListener.onDeviceMessage(message);

    // Verify
    verify(blockchainConnectorService).publishMessage(message);
  }
}
