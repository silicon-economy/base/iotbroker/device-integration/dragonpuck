#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=iot-broker}"
FULLNAME="dragonpuck"

# Upgrade or install
helm --set-string="fullnameOverride=${FULLNAME}" upgrade -n "$NAMESPACE" -i dragonpuck . || exit 1
# Ensure image stream picks up the new docker image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-adapter"
oc -n "$NAMESPACE" import-image "${FULLNAME}-connector"
oc -n "$NAMESPACE" import-image "${FULLNAME}-blockchain-adapter"
