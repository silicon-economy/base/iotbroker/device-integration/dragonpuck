# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.0] - 2023-09-22
### Changed
- Update the DragonPuck Adapter and Connector to be based on v2.7.1 of their SensingPuck counterparts.
  This primarily updates IoT Broker dependencies.
- Update the URL to the Device Registry Service's web API, which has been extended by a '/v1' suffix with v2.0.0 of the Device Registry Service.

## [2.0.1] - 2023-05-03
### Changed
- Relicense under the OLFL-1.3.

## [2.0.0] - 2023-02-15
### Added
- Add a DragonPuck Blockchain Adapter application responsible for sending (raw) DragonPuck messages to the Blockchain Connector.
  This functionality was previously included in the DragonPuck Adapter.

### Changed
- Adjust the deployment of the DragonPuck Adapter and Connector to be based on the corresponding SensingPuck docker images.
  As a result, the DragonPuck Adapter and Connector are now compatible with the latest SensingPuck firmware, which among other things introduces a new protocol version and supports encrypted communication.
  DragonPuck Adapter and Connector are now based on v2.4.2 of their SensingPuck counterparts.

### Removed
- Remove the DragonPuck-specific adapter and connector implementations and remove the common project.
  With the DragonPuck Adapter and Connector now being based on their SensingPuck counterparts, these implementations are no longer used.

## [1.1.3] - 2022-04-01
### Changed
- Update Spring Boot to v2.5.12.
  This Spring Boot version addresses CVE-2022-22965.

## [1.1.2] - 2022-01-21
### Changed
- Update Spring Boot to v2.5.9.
  This Spring Boot version includes Log4j v2.17.1, which fixes a number of vulnerabilities that have been reported in recent weeks.
  For more information on the reported vulnerabilities, see the following links:
  - CVE-2021-44228, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228
  - CVE-2021-45046, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046
  - CVE-2021-45105, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105
  - CVE-2021-44832, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

## [1.1.1] - 2021-12-14
### Changed
- Update Log4j dependencies (especially `log4j-core`) to v2.15.0.
  A vulnerability has been reported with CVE-2021-44228 against the `log4j-core` jar and has been fixed in Log4j v2.15.0.
  Until Log4j v2.15.0 is picked up by Spring Boot, which is planned for the Spring Boot v2.5.8 & v2.6.2 releases (due Dec 23, 2021), the Log4j dependencies are overridden manually.
  For more information on CVE-2021-44228, see https://nvd.nist.gov/vuln/detail/CVE-2021-44228.

## [1.1.0] - 2021-11-24
### Added
- Dragon Puck Connector: route `/devices/{deviceID}/jobs/finish` to set all jobs of a given
  device to the state `Finished`.

### Changed
- Adjust exception handling in the Dragon Puck Connector's web API, resulting in more expressive error responses for clients.
- Adjust handling of active jobs and allow only one active job to be assigned to a device at a time.
  This rule is now also taken into account in the DragonPuck Connector's web API when creating new jobs or updating existing ones.

## [1.0.1] - 2021-11-17
### Added
- This CHANGELOG file to keep track of the changes in this project.

### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [1.0.0] - 2021-10-14
This is the first public release.

The device integration for Dragon Puck devices adds the following abilities to the Iot Broker:

- Communication with Dragon Puck devices
- Integration with the IoT Broker's core services
- Configuration of Dragon Puck devices
- Query Dragon Puck messages

This release corresponds to IoT Broker v0.9.
